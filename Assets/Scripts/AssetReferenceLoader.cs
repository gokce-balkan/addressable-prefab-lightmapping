﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

public class AssetReferenceLoader : MonoBehaviour
{
    [SerializeField] private List<AssetReference> assetReferenceList;
    [SerializeField] private List<AssetLabelReference> assetLabels;
    [SerializeField] private List<GameObject> loadedObjects;
    List<AsyncOperationHandle<GameObject>> asyncOperations;
    List<IList<IResourceLocation>> resourceLocationLists = new List<IList<IResourceLocation>>();

    private void Start()
    {
        Debug.Log("starting to load");
        Debug.Log(Addressables.RuntimePath);
        loadedObjects = new List<GameObject>();
        asyncOperations = new List<AsyncOperationHandle<GameObject>>();
        CreateAssetList();
    }

    private IEnumerator LaunchObjects()
    {
        for (int i = 0; i < resourceLocationLists.Count; ++i)
        {
            yield return StartCoroutine(AddObjectsToScene(resourceLocationLists[i]));
        }
    }

    public async void CreateAssetList()
    {
        for (int i = 0; i < assetLabels.Count; ++i)
        {
            var assets = await Addressables.LoadResourceLocationsAsync(assetLabels[i]).Task;
            resourceLocationLists.Add(assets);
            Debug.Log("asset locations for " + assetLabels[i].labelString + " retrieved");
            Debug.Log(assetLabels[i].RuntimeKey);
        }
        StartCoroutine(LaunchObjects());
    }

    private IEnumerator AddObjectsToScene(IList<IResourceLocation> assetList)
    {
        for (int i = 0; i < assetList.Count; ++i)
        {
            Debug.Log("instantiating " + assetList[i].ToString());
            yield return StartCoroutine(AddObjectToScene(assetList[i]));
        }
    }

    private IEnumerator AddObjectToScene(IResourceLocation location)
    {
        AsyncOperationHandle<GameObject> asyncOperation = Addressables.LoadAssetAsync<GameObject>(location);
        asyncOperation.Completed += OnCompleted;
        asyncOperations.Add(asyncOperation);
        while (!asyncOperation.IsDone)
        {
            Debug.Log(asyncOperation.DebugName + " " + asyncOperation.PercentComplete + "% complete");
            yield return null;
        }
    }

    private void OnCompleted(AsyncOperationHandle<GameObject> obj)
    {
        Instantiate(obj.Result);
        Debug.Log(obj.Result.name + " instantiated");
        asyncOperations.Remove(obj);
    }
}
